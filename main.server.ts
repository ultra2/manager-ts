declare var Promise: any;
import g from './global.server'
import Component from './component.server'

import * as fsextra from 'fs-extra'
import * as pathhelper from 'path'

export default class Navigator extends Component<any, any> {

    constructor() {
        super()
        g.components["navigator"] = this
    }

    getApplications(data: any) {   
        var livePath = "/tmp/live"
        var applications = []

        fsextra.ensureDir(livePath)
        var directories = fsextra.readdirSync(livePath)
        for (var i in directories) {
            var dir = directories[i]
            if (!fsextra.lstatSync(pathhelper.join(livePath, dir)).isDirectory()) continue
            applications.push(dir)
        } 

        this.emit('main:applications', applications)
    }

    install(data: any){
        this.send("install", data)
    }

    uninstall(data: any){
        this.send("uninstall", data)
    }

    update(data: any){
        this.send("update", data)
    }

    start(data: any){
        debugger
        this.send("start", data)
    }

    restart(data: any){
        this.send("restart", data)
    }
}