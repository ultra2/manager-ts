//#COMMON
declare var Promise: any
import g from './global.client'
import Component from './component.client'
import Auth from './auth.client'
//#COMMON END

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Panel, Form, Table, Grid, Row, Col, Label, Button, Navbar, Nav, NavItem, NavDropdown, MenuItem, FormGroup, FormControl, ControlLabel, Glyphicon } from 'react-bootstrap';

export interface IMainProps {}
export class Main extends Component<IMainProps, any> {

    state = {
        name: "",
        url: "",
        accessToken: "",
        applications: []
    }
 
    constructor (props) {
        super(props)
        g.components["main"] = this

        this.onNameChanged = this.onNameChanged.bind(this)
        this.onUrlChanged = this.onUrlChanged.bind(this)
        this.onAccessTokenChanged = this.onAccessTokenChanged.bind(this)

        this.install = this.install.bind(this)
        this.uninstall = this.uninstall.bind(this)
        this.update = this.update.bind(this)
        this.restart = this.restart.bind(this)

        this.emit('main:getApplications', null)
    }

    applications(msg){
        this.state.applications = msg
        this.setState(this.state)
    }

    install(){
        this.emit('main:install', { app: this.state.name, url: this.state.url, accessToken: this.state.accessToken })
    }

    uninstall(app: string){
        this.emit('main:uninstall', { app: app })
    }

    update(app: string){
        this.emit('main:update', { app: app })
    }

    restart(app: string){
        this.emit('main:restart', { app: app })
    }

    start(app: string){
        this.emit('main:start', { app: app })
    }

    open(app: string){
        var protocol = window.location.protocol
        var host = window.location.host
        host = app + host.substr(host.indexOf('.'))
        var url = protocol + '//' + host
        var win = window.open(url, '_blank');
        win.focus()
    }

    onNameChanged(e) {
        this.state.name = e.target.value
        this.setState(this.state)
    }

    onUrlChanged(e) {
        this.state.url = e.target.value
        this.setState(this.state)
    }

    onAccessTokenChanged(e) {
        this.state.accessToken = e.target.value
        this.setState(this.state)
    }

    render () {
        const rowApp = this.state.applications.map((app) =>
            <tr>
                <td>{app}</td>
                <td></td>
                <td></td>
                <td>
                    <Button bsStyle="danger" onClick={()=>this.uninstall(app)}><Glyphicon glyph="trash" /></Button>
                    {' '}
                    <Button bsStyle="success" onClick={()=>this.update(app)}><Glyphicon glyph="refresh" /> Update</Button>
                    {' '}
                    <Button bsStyle="success" onClick={()=>this.restart(app)}><Glyphicon glyph="repeat" /> Restart</Button>
                    {' '}
                    <Button bsStyle="success" onClick={()=>this.start(app)}><Glyphicon glyph="repeat" /> Start</Button>
                    {' '}
                    <Button bsStyle="success" onClick={()=>this.open(app)}><Glyphicon glyph="play-circle" /> Open</Button>
                </td>
            </tr>
        )

        return (
            <Panel>
                <Auth />
                
                <Panel header="Install new application">
                    <FormGroup>
                        <ControlLabel>Name</ControlLabel>
                        <FormControl style={{ width: '20%' }} type="text" value={this.state.name} placeholder="Name" onChange={this.onNameChanged} />
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Url</ControlLabel>
                        <FormControl style={{ width: '40%' }} type="text" value={this.state.url} placeholder="Url" onChange={this.onUrlChanged} />
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Access Token</ControlLabel>
                        <FormControl  style={{ width: '20%' }} type="text" value={this.state.accessToken} placeholder="Access Token" onChange={this.onAccessTokenChanged} />
                    </FormGroup>
                    <FormGroup>
                        <Button bsStyle="success" onClick={this.install}><Glyphicon glyph="floppy-save" /> Install</Button>
                    </FormGroup>
                </Panel>

                <Panel header="Installed applications">
                    <Table striped bordered condensed hover>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Repository</th>
                                <th>Version</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {rowApp}
                        </tbody>
                    </Table>
                </Panel>
            </Panel>
        )
    }
}

ReactDOM.render(React.createElement(Main), document.getElementById('root'))
