declare var Promise: any;
import g from './global.client';
import Component from './component.client';

import * as React from 'react';
import { Label, Button, Navbar, Nav, NavItem, NavDropdown, MenuItem, FormGroup, FormControl, ControlLabel, Glyphicon } from 'react-bootstrap';

export default class Auth extends Component<any, any> {

  state = {
    authenticated: false
  }

  constructor(props) {
    super(props)
    g.components["auth"] = this
  }

  onLogin(){
    var password = prompt("password: ")
    this.emit('auth:login', {username: "John", password: password})
  }

  authenticated(msg){
    this.state.authenticated = msg.success
    this.setState(this.state)
  }

  unauthorized(msg){
    alert("unauthorized")
  }

  render () {

    return (
      <div>
          {this.state.authenticated ? (
            <Button bsStyle="success" onClick={()=>this.onLogin()}><Glyphicon glyph="refresh" /> Logout</Button>
          ) : (
            <Button bsStyle="success" onClick={()=>this.onLogin()}><Glyphicon glyph="refresh" /> Login</Button>
          )}
      </div>
    )
  }
}