"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const global_server_1 = require("./global.server");
const component_server_1 = require("./component.server");
class Auth extends component_server_1.default {
    constructor() {
        super();
        global_server_1.default.components["auth"] = this;
    }
    login(data) {
        var success = data.password == "a";
        this.socket.auth = success;
        this.emit('auth:authenticated', { success: success });
    }
}
exports.default = Auth;
