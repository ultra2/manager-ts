"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const global_server_1 = require("./global.server");
const component_server_1 = require("./component.server");
const fsextra = require("fs-extra");
const pathhelper = require("path");
class Navigator extends component_server_1.default {
    constructor() {
        super();
        global_server_1.default.components["navigator"] = this;
    }
    getApplications(data) {
        var livePath = "/tmp/live";
        var applications = [];
        fsextra.ensureDir(livePath);
        var directories = fsextra.readdirSync(livePath);
        for (var i in directories) {
            var dir = directories[i];
            if (!fsextra.lstatSync(pathhelper.join(livePath, dir)).isDirectory())
                continue;
            applications.push(dir);
        }
        this.emit('main:applications', applications);
    }
    install(data) {
        this.send("install", data);
    }
    uninstall(data) {
        this.send("uninstall", data);
    }
    update(data) {
        this.send("update", data);
    }
    start(data) {
        debugger;
        this.send("start", data);
    }
    restart(data) {
        this.send("restart", data);
    }
}
exports.default = Navigator;
