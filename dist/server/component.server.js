"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//#COMMON END
class Component {
    emit(message, data, component) {
        //if (component) message = component + ":" + message
        //this.emitfn(message, data)
        this.socket.emit(message, data);
    }
    log(message) {
        this.emit("log:write", message);
    }
    send(command, data) {
        if (!process.send) {
            console.log("no engine, cant send: ", command, data);
        }
        process.send({ command: command, data: data });
    }
}
exports.default = Component;
