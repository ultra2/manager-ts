var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("global.client", ["require", "exports", "socket.io-client"], function (require, exports, io) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Global = (function () {
        function Global() {
            if (Global._instance) {
                throw new Error("The Global is a singleton class and cannot be created!");
            }
            Global._instance = this;
            this.components = {};
            this.socket = io.connect();
            var onevent = this.socket.onevent;
            this.socket.onevent = function (packet) {
                var args = packet.data || [];
                onevent.call(this, packet); // original call
                packet.data = ["*"].concat(args);
                onevent.call(this, packet); // additional call to catch-all
            };
            this.socket.on("*", function (message, data) {
                var splittedMessage = message.split(':');
                var component = splittedMessage[0];
                var method = splittedMessage[1];
                var componentInstance = this.components[component];
                if (componentInstance != null) {
                    componentInstance[method](data);
                }
            }.bind(this));
        }
        Global.getInstance = function () {
            return Global._instance;
        };
        Global._instance = new Global();
        return Global;
    }());
    var g = Global.getInstance();
    exports.default = g;
});
define("component.client", ["require", "exports", "global.client", "react"], function (require, exports, global_client_1, React) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Component = (function (_super) {
        __extends(Component, _super);
        function Component(props) {
            return _super.call(this, props) || this;
        }
        Component.prototype.emit = function (message, data, component) {
            //if (component) message = component + ":" + message
            global_client_1.default.socket.emit(message, data);
        };
        return Component;
    }(React.Component));
    exports.default = Component;
});
define("auth.client", ["require", "exports", "global.client", "component.client", "react", "react-bootstrap"], function (require, exports, global_client_2, component_client_1, React, react_bootstrap_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Auth = (function (_super) {
        __extends(Auth, _super);
        function Auth(props) {
            var _this = _super.call(this, props) || this;
            _this.state = {
                authenticated: false
            };
            global_client_2.default.components["auth"] = _this;
            return _this;
        }
        Auth.prototype.onLogin = function () {
            var password = prompt("password: ");
            this.emit('auth:login', { username: "John", password: password });
        };
        Auth.prototype.authenticated = function (msg) {
            this.state.authenticated = msg.success;
            this.setState(this.state);
        };
        Auth.prototype.unauthorized = function (msg) {
            alert("unauthorized");
        };
        Auth.prototype.render = function () {
            var _this = this;
            return (React.createElement("div", null, this.state.authenticated ? (React.createElement(react_bootstrap_1.Button, { bsStyle: "success", onClick: function () { return _this.onLogin(); } },
                React.createElement(react_bootstrap_1.Glyphicon, { glyph: "refresh" }),
                " Logout")) : (React.createElement(react_bootstrap_1.Button, { bsStyle: "success", onClick: function () { return _this.onLogin(); } },
                React.createElement(react_bootstrap_1.Glyphicon, { glyph: "refresh" }),
                " Login"))));
        };
        return Auth;
    }(component_client_1.default));
    exports.default = Auth;
});
define("main.client", ["require", "exports", "global.client", "component.client", "auth.client", "react", "react-dom", "react-bootstrap"], function (require, exports, global_client_3, component_client_2, auth_client_1, React, ReactDOM, react_bootstrap_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Main = (function (_super) {
        __extends(Main, _super);
        function Main(props) {
            var _this = _super.call(this, props) || this;
            _this.state = {
                name: "",
                url: "",
                accessToken: "",
                applications: []
            };
            global_client_3.default.components["main"] = _this;
            _this.onNameChanged = _this.onNameChanged.bind(_this);
            _this.onUrlChanged = _this.onUrlChanged.bind(_this);
            _this.onAccessTokenChanged = _this.onAccessTokenChanged.bind(_this);
            _this.install = _this.install.bind(_this);
            _this.uninstall = _this.uninstall.bind(_this);
            _this.update = _this.update.bind(_this);
            _this.restart = _this.restart.bind(_this);
            _this.emit('main:getApplications', null);
            return _this;
        }
        Main.prototype.applications = function (msg) {
            this.state.applications = msg;
            this.setState(this.state);
        };
        Main.prototype.install = function () {
            this.emit('main:install', { app: this.state.name, url: this.state.url, accessToken: this.state.accessToken });
        };
        Main.prototype.uninstall = function (app) {
            this.emit('main:uninstall', { app: app });
        };
        Main.prototype.update = function (app) {
            this.emit('main:update', { app: app });
        };
        Main.prototype.restart = function (app) {
            this.emit('main:restart', { app: app });
        };
        Main.prototype.start = function (app) {
            this.emit('main:start', { app: app });
        };
        Main.prototype.open = function (app) {
            var protocol = window.location.protocol;
            var host = window.location.host;
            host = app + host.substr(host.indexOf('.'));
            var url = protocol + '//' + host;
            var win = window.open(url, '_blank');
            win.focus();
        };
        Main.prototype.onNameChanged = function (e) {
            this.state.name = e.target.value;
            this.setState(this.state);
        };
        Main.prototype.onUrlChanged = function (e) {
            this.state.url = e.target.value;
            this.setState(this.state);
        };
        Main.prototype.onAccessTokenChanged = function (e) {
            this.state.accessToken = e.target.value;
            this.setState(this.state);
        };
        Main.prototype.render = function () {
            var _this = this;
            var rowApp = this.state.applications.map(function (app) {
                return React.createElement("tr", null,
                    React.createElement("td", null, app),
                    React.createElement("td", null),
                    React.createElement("td", null),
                    React.createElement("td", null,
                        React.createElement(react_bootstrap_2.Button, { bsStyle: "danger", onClick: function () { return _this.uninstall(app); } },
                            React.createElement(react_bootstrap_2.Glyphicon, { glyph: "trash" })),
                        ' ',
                        React.createElement(react_bootstrap_2.Button, { bsStyle: "success", onClick: function () { return _this.update(app); } },
                            React.createElement(react_bootstrap_2.Glyphicon, { glyph: "refresh" }),
                            " Update"),
                        ' ',
                        React.createElement(react_bootstrap_2.Button, { bsStyle: "success", onClick: function () { return _this.restart(app); } },
                            React.createElement(react_bootstrap_2.Glyphicon, { glyph: "repeat" }),
                            " Restart"),
                        ' ',
                        React.createElement(react_bootstrap_2.Button, { bsStyle: "success", onClick: function () { return _this.start(app); } },
                            React.createElement(react_bootstrap_2.Glyphicon, { glyph: "repeat" }),
                            " Start"),
                        ' ',
                        React.createElement(react_bootstrap_2.Button, { bsStyle: "success", onClick: function () { return _this.open(app); } },
                            React.createElement(react_bootstrap_2.Glyphicon, { glyph: "play-circle" }),
                            " Open")));
            });
            return (React.createElement(react_bootstrap_2.Panel, null,
                React.createElement(auth_client_1.default, null),
                React.createElement(react_bootstrap_2.Panel, { header: "Install new application" },
                    React.createElement(react_bootstrap_2.FormGroup, null,
                        React.createElement(react_bootstrap_2.ControlLabel, null, "Name"),
                        React.createElement(react_bootstrap_2.FormControl, { style: { width: '20%' }, type: "text", value: this.state.name, placeholder: "Name", onChange: this.onNameChanged })),
                    React.createElement(react_bootstrap_2.FormGroup, null,
                        React.createElement(react_bootstrap_2.ControlLabel, null, "Url"),
                        React.createElement(react_bootstrap_2.FormControl, { style: { width: '40%' }, type: "text", value: this.state.url, placeholder: "Url", onChange: this.onUrlChanged })),
                    React.createElement(react_bootstrap_2.FormGroup, null,
                        React.createElement(react_bootstrap_2.ControlLabel, null, "Access Token"),
                        React.createElement(react_bootstrap_2.FormControl, { style: { width: '20%' }, type: "text", value: this.state.accessToken, placeholder: "Access Token", onChange: this.onAccessTokenChanged })),
                    React.createElement(react_bootstrap_2.FormGroup, null,
                        React.createElement(react_bootstrap_2.Button, { bsStyle: "success", onClick: this.install },
                            React.createElement(react_bootstrap_2.Glyphicon, { glyph: "floppy-save" }),
                            " Install"))),
                React.createElement(react_bootstrap_2.Panel, { header: "Installed applications" },
                    React.createElement(react_bootstrap_2.Table, { striped: true, bordered: true, condensed: true, hover: true },
                        React.createElement("thead", null,
                            React.createElement("tr", null,
                                React.createElement("th", null, "Name"),
                                React.createElement("th", null, "Repository"),
                                React.createElement("th", null, "Version"),
                                React.createElement("th", null))),
                        React.createElement("tbody", null, rowApp)))));
        };
        return Main;
    }(component_client_2.default));
    exports.Main = Main;
    ReactDOM.render(React.createElement(Main), document.getElementById('root'));
});
