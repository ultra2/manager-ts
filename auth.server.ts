import g from './global.server';
import Component from './component.server';

export default class Auth extends Component<any, any> {

    constructor() {
        super()
        g.components["auth"] = this
    }

    login(data: any) {  
        var success = data.password == "a"
        this.socket.auth = success
        this.emit('auth:authenticated', { success: success })
    } 
}
