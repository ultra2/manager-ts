//#COMMON
declare var Promise: any
import g from './global.client'
//#COMMON END

import * as React from 'react'

export default class Component<P, S> extends React.Component<P, S> {

    constructor (props) {
        super(props)
    }

    public emit(message: string, data: any, component?: string){
        //if (component) message = component + ":" + message
        g.socket.emit(message, data)
    }
}
